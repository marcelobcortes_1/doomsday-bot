<?php

require __DIR__ . '/vendor/autoload.php';
require __DIR__.'/config.php';
include('AsteroidInfo/asteroid.php');
include('Subscription/notification.php');
include('Subscription/subscribe.php');

$file = __DIR__ . '/output.txt';

$asteroid = new AsteroidInformation();

if ($asteroid->getHazardousAsteroidsCount() > 0){
    $notification = new Notification($asteroid);
    $notification->send();
}

?>
