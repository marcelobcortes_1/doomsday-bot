# Beware! Asteroids are coming

Este é um bot para Telegram que informa dados sobre asteróides potencialmente perigosos ao planeta Terra para a data atual. Além de informar, permite que o usuário se inscrever para receber notificação diária via Telegram e E-mail, caso houver asteróides ameaçadores.

#### Bibliotecas e serviços externos

  - No projeto, é utilizado o pacote longman/telegram-bot que fornece estrutura e métodos convenientes para a criação do bot;
  - Pacote GuzzleHttp para realizar requisição à API com os dados dos asteróides;
  - MailChimp é utilizado para realizar o disparo dos e-mails, bem como o pacote drewm/mailchimp-api que abstrai a utilização da API do serviço. Este serviço é útil por fornecer confiabilidade na entrega dos e-mails, automatizando detalhes no envio de e-mails;
  - Banco de dados MySQL pela facilidade do uso. O banco de dados é responsável por guardar as informações sobre os asteróides e dos inscritos.
  - Crontab para realizar o agendamento do disparo de e-mails;
  - Ngrok para simplificar o processo de desenvolvimento ao fornecer HTTPS (necessário no webhook) para o servidor localhost.



#### Instalação

O bot depende de PHP, Composer, MySQL e Crontab.

Rode o comando na pasta principal:

```sh
$ composer install
```

No banco de dados, crie um database e rode o script SQL do arquivo "schema.sql"

Adquira uma API Token em https://mailchimp.com

Renomeie o arquivo config.example.php para config.php e altere as seguintes variáveis:

>>$token
>>$username
>>$base_url
>>$mysql_host
>>$mysql_user
>>$mysql_pass
>>$mysql_db
>>$api_token
>>$mailchimp_token

Inclua um cron job para realizar o disparo de e-mails.

```sh
$ crontab -e
```
Exemplo de cron job:
>>0 11 * * * php -q /var/www/html/bot/cron.php 2>> /var/www/html/bot/log/err.log

Neste exemplo, os e-mails serão disparados todos os dias às 11 a.m. Você deve conferir o caminho do arquivo cron.php.
