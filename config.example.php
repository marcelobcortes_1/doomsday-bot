<?php
// telegram data
$token = "telegram_bot_token";
$username = "telegram_bot_username";

// where is your php server?
$base_url = "https://yourdomain.com/";

// mysql server
$mysql_host = "127.0.0.1";
$mysql_user = "root";
$mysql_pass = "";
$mysql_db = "bot_database_name";

// NASA's api token
$api_token = "nasa_api_token";

// mail chimp's api token
$mailchimp_token = "mailchimp_api_token";


//
$mailchimp_list_config = [
    "name" => "Asteroids",
    "permission_reminder" => "You are subscribed to Telegram's bot Beware! Asteroids are coming",
    "email_type_option" => true,
    "campaign_defaults" => [
        "from_name" => "Beware! Asteroids are coming",
        "from_email" => "beware@asteroids.com",
        "subject" => "Hazardous asteroids",
        "language" => "pt-br"
    ],
    "contact" => [
        "company" => "Marcelo TI",
        "address1" => "123 Fake Address Street",
        "city" => "São Paulo",
        "state" => "São Paulo",
        "zip" => "01001-001",
        "country" => "Brazil"
    ]
];

$con = new mysqli($mysql_host, $mysql_user, $mysql_pass, $mysql_db);
if ($con->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

?>