<?php
/**
 * This file is part of the TelegramBot package.
 *
 * (c) Avtandil Kikabidze aka LONGMAN <akalongman@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Longman\TelegramBot\Commands\SystemCommands;

use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Request;

/**
 * Start command
 */
class StartCommand extends SystemCommand
{
    /**
     * @var string
     */
    protected $name = 'start';

    /**
     * @var string
     */
    protected $description = 'Start command';

    /**
     * @var string
     */
    protected $usage = '/start';

    /**
     * @var string
     */
    protected $version = '1.0.0';

    /**
     * Command execute method
     *
     * @return mixed
     * @throws \Longman\TelegramBot\Exception\TelegramException
     */
    public function execute()
    {
        $message = $this->getMessage();

        $msg = "Pergunte-me:\nAlgum asteroide oferece perigo ao planeta Terra na data de hoje?";
        $msg = $msg . "\n\nUse o comando /subscribe para receber notificação diária, caso houver asteróides ameaçadores.";
        $msg = $msg . "\nInclua seu e-mail na frente do comando para ser notificado também via e-mail.";
        
        return Request::sendMessage([
            'chat_id' => $message->getChat()->getId(),
            'text'    => $msg,
        ]);
    }
}
