<?php

namespace Longman\TelegramBot\Commands\SystemCommands;

include_once('AsteroidInfo/asteroid.php');

use Longman\TelegramBot\Request;
use Longman\TelegramBot\Commands\SystemCommand;
use AsteroidInformation;

class GenericmessageCommand extends SystemCommand
{

    public function execute()
    {
        $message = $this->getMessage();
        $asteroid = new AsteroidInformation();

        $q = "Algum asteroide oferece perigo ao planeta Terra na data de hoje?";
        if (strcmp($q, $message->getText()) == 0){    
            return Request::sendMessage([
                'chat_id' => $message->getChat()->getId(),
                'text'    => $asteroid->getAsteroidsMessage()
            ]);
        }

        return Request::emptyResponse();
    }

}
