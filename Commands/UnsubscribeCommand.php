<?php

namespace Longman\TelegramBot\Commands\SystemCommands;

use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Request;
use Subscription;

include_once('Subscription/subscribe.php');

class UnsubscribeCommand extends SystemCommand
{
    protected $name = 'unsubscribe';

    public function execute()
    {
        $message = $this->getMessage();
        $chat_id = $message->getChat()->getId();

        $sub = new Subscription();

        if ($sub->doUnsubscribe($chat_id)){
            $msg = "Você cancelou as notificações de asteróides perigosos.";
        } else {
            $msg = "Ops! Algo de errado não está certo.";
        }
    
        return Request::sendMessage([
            'chat_id' => $chat_id,
            'text'    => $msg,
        ]);
    }
}
