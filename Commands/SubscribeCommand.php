<?php

namespace Longman\TelegramBot\Commands\SystemCommands;

use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Request;
use Subscription;

include_once('Subscription/subscribe.php');

class SubscribeCommand extends SystemCommand
{
    protected $name = 'subscribe';

    public function execute()
    {
        $message = $this->getMessage();
        $chat_id = $message->getChat()->getId();

        $sub = new Subscription();
        
        $msg = "";
    
        if ($sub->doSubscribe($message->getText(true), $chat_id)){
            $msg = "Você receberá alerta de asteróides potencialmente perigosos.\nDigite /unsubscribe para desfazer.";
        } else {
            $msg = "Ops! Algo de errado não está certo.";
        }
    
        return Request::sendMessage([
            'chat_id' => $chat_id,
            'text'    => $msg,
        ]);
    }
}
