<?php

use Phpfastcache\Config\ConfigurationOption;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

require_once __DIR__.'/../config.php';

class AsteroidInformation {

    private $api_token = NULL;
    private $con = NULL;

    public function __construct() {  
        global $api_token;
        global $con;
        
        $this->con = $con;
        $this->api_token = $api_token;
    }

    public function getAsteroidsMessage()
    {
        if ($this->getHazardousAsteroidsCount() == 0) {
            return "Não, fique tranquilo ;-)";
        }

        return $this->getMessage();
    }

    public function getHazardousAsteroidsCount()
    {
        return count($this->getInfo());
    }


    public function getAsteroidsMessageHTML()
    {
        $data = $this->getInfo();

        $str = "<h3>Estamos ameaçados ";
        $str = $str . (($this->getHazardousAsteroidsCount()>1) ? "pelos seguintes asteróides:</h3>" : "pelo seguinte asteróide:</h3>");

        foreach($data as $a){
            $str = $str . "<br><p><b>Nome: " . $a[0];
            
            $str = $str . "</b></p><p>" . "Diâmetro estimado entre " . number_format($a[1], 2, ',', '.') . " a " . number_format($a[2], 2, ',', '.') . " km.";
            $str = $str . "</p><p>" . "Velocidade relativa à Terra: " . number_format($a[3], 0, ',','.') . " km/h.";
            $str = $str . "</p><p>" . "Menor distância entre a Terra: " . number_format($a[4], 0, ',','.') . " km.";
            $str = $str . "</p><p>" . "Saiba mais em: " . $a[5] . "</p>";     
        }
        return $str;
    }

    private function getMessage()
    {
        $data = $this->getInfo();
        $str = "Sim!";
        
        foreach($data as $a){
            $str = $str . "\n\nNome: " . $a[0];
            
            $str = $str . "\n" . "Diâmetro estimado entre " . number_format($a[1], 2, ',', '.') . " a " . number_format($a[2], 2, ',', '.') . " km.";
            $str = $str . "\n" . "Velocidade relativa à Terra: " . number_format($a[3], 0, ',','.') . " km/h.";
            $str = $str . "\n" . "Menor distância entre a Terra: " . number_format($a[4], 0, ',','.') . " km.";
            $str = $str . "\n" . "Saiba mais em: " . $a[5];
        }

        return $str;
    }

    private function getInfo()
    {  
        $date = date("Y-m-d");

        $q = "select name, diameter_min, diameter_max, miss_distance, relative_velocity, link from asteroids where date = '" . $date . "'";
        $r = $this->con->query($q);

        if($r->num_rows == 0){
            $this->getHazardousAsteroidsInfo();
            return $this->getInfo();
        }else{
            return $r->fetch_all();
        }
    }

    private function getHazardousAsteroidsInfo()
    {
        $r = $this->fetchDataFromAPI();
        $date = date("Y-m-d");
        
        $q = "";

        foreach ($r->near_earth_objects as $d){
            foreach ($d as $a){
                if ($a->is_potentially_hazardous_asteroid == true){
                    if ($q == "")
                    {
                        $q = "INSERT INTO asteroids (date, name, diameter_min, diameter_max, miss_distance, relative_velocity, link) VALUES ('";
                    }

                    $q = $q . $date . "', '";
                    $q = $q . $a->name . "', '";
                    $q = $q . $a->estimated_diameter->kilometers->estimated_diameter_min . "', '";
                    $q = $q . $a->estimated_diameter->kilometers->estimated_diameter_max . "', '";
                    $q = $q . $a->close_approach_data[0]->miss_distance->kilometers . "', '";
                    $q = $q . $a->close_approach_data[0]->relative_velocity->kilometers_per_hour . "', '";
                    $q = $q . $a->nasa_jpl_url . "'), ('";
                }
            }
        }
        $q = substr($q, 0, -4);

        $this->con->query($q);
    }

    private function fetchDataFromAPI()
    {
        $client = new GuzzleHttp\Client();

        $res = $client->request('GET', $this->buildUrlCall(),
        [
            'headers' => [
            'Accept' => 'application/json',
            'Content-type' => 'application/json'
        ]]);

        $r = json_decode($res->getBody()->getContents());
        
        return $r;
    }

    private function buildUrlCall(){
        $date = date("Y-m-d");

        return "https://api.nasa.gov/neo/rest/v1/feed?start_date=" . $date . "&end_date=" . $date . "&detailed=false&api_key=" . $this->api_token;
    }
}
?>