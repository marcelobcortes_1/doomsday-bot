<?php
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/config.php';

$bot_api_key  = $token;
$bot_username = $username;

$commands_folder = ['./Commands/'];

try {
    $telegram = new Longman\TelegramBot\Telegram($bot_api_key, $bot_username);
        
    $telegram->enableLimiter();

    $telegram->addCommandsPaths($commands_folder);

    $telegram->handle();
} catch (Longman\TelegramBot\Exception\TelegramException $e) {
    echo $e->getMessage();
}
