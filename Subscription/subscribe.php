<?php
use Phpfastcache\Config\ConfigurationOption;
use \DrewM\MailChimp\MailChimp;

require_once __DIR__.'/../config.php';

class Subscription {

    private $MailChimp = null;
    private $mailchimp_list_config = null;
    private $con = null;

    public function __construct()
    {
        global $con;
        global $mailchimp_token;
        global $mailchimp_list_config;

        $this->con = $con;
        $this->MailChimp = new MailChimp($mailchimp_token);
        $this->mailchimp_list_config = $mailchimp_list_config;
    }

    public function doSubscribe($message, $chat_id)
    {  
        if (!$this->include($chat_id)){
            return false;
        }

        if (filter_var(trim($message), FILTER_VALIDATE_EMAIL)) {
            return $this->includeInEmailList($chat_id, trim($message));
        }
        return true;
    }

    public function doUnsubscribe($chat_id)
    {
        if ($subId = $this->getSubscriptionMailChimpId($chat_id)){
            $this->excludeFromEmailList($subId);
        }
        return $this->exclude($chat_id);
    }

    private function include($chat_id)
    {
        $q = "SELECT id FROM subscriptions WHERE chat_id = '" . $chat_id . "'";
        $r = $this->con->query($q);

        if ($r->num_rows > 0)
        {
            return false;
        }

        $q = "INSERT INTO subscriptions (chat_id) VALUES ('" . $chat_id . "')";
        $this->con->query($q);
        return true;
    }

    private function exclude($chat_id)
    {
        $q = "SELECT id FROM subscriptions WHERE chat_id = '" . $chat_id . "'";
        $r = $this->con->query($q);

        if ($r->num_rows == 0)
        {
            return false;
        }

        $q = "DELETE FROM subscriptions WHERE chat_id = '" . $chat_id . "'";
        $this->con->query($q);
        return true;
    }

    private function getSubscriptionMailChimpId($chat_id)
    {
        $q = "SELECT sub_id FROM subscriptions WHERE chat_id = '" . $chat_id . "'";
        $r = $this->con->query($q);

        if ($r->num_rows == 0)
        {
            return false;
        } else {
            $r = $r->fetch_assoc();
            return $r["sub_id"] == null ? false : $r["sub_id"];
        }
    }

    private function updateDatabaseWithMailChimpId($chat_id, $subId)
    {
        $q = "UPDATE subscriptions SET sub_id = '" . $subId . "' WHERE chat_id = '" . $chat_id . "'";
        $this->con->query($q);
    }

    private function includeInEmailList($chat_id, $email)
    {
        $emailList = $this->getEmailList();

        if ($emailList){
            $result = $this->MailChimp->post("lists/$emailList/members", [
                'email_address' => $email,
                'status'        => 'subscribed',
            ]);

            if ($this->MailChimp->success()){
                $this->updateDatabaseWithMailChimpId($chat_id, $result['id']);
                return true;
            } else {
                error_log($this->MailChimp->getLastError());
            }
        }
        return false;
    }

    private function excludeFromEmailList($subId)
    {
        $emailList = $this->getEmailList();

        $this->MailChimp->delete("lists/$emailList/members/$subId");
    }

    public function getEmailList()
    {
        $q = "SELECT v FROM key_val WHERE k = 'emailList'";
        $r = $this->con->query($q);

        if ($r->num_rows == 0)
        {
            $this->createEmailList();
            return $this->getEmailList();
        }

        $r = $r->fetch_assoc();
        return $r['v'];
    }

    private function createEmailList()
    {
        $result = $this->MailChimp->post("lists", $this->mailchimp_list_config);

        if ($this->MailChimp->success()){
            $q = "INSERT INTO key_val (k, v) VALUES ('emailList', '" . $result['id'] . "')";
            $this->con->query($q);
            return $result['id'];
        }
        error_log($this->MailChimp->getLastError());
        return false;
    }

    public function hasEmailSubscriptors()
    {
        $emailList = $this->getEmailList();
        $result = $this->MailChimp->get("lists/$emailList/members");

        if ($this->MailChimp->success()){
            return $result['total_items'] > 0;
        } else {
            return false;
        }
    }

    public function getSubscriptors()
    {
        $q = "SELECT chat_id, sub_id FROM subscriptions";
        $r = $this->con->query($q);

        if ($r->num_rows == 0)
        {
            return [];
        }

        $s = array();
        $r = $r->fetch_all();

        foreach($r as $sub){
            array_push($s, $sub[0]);
        }
        return $s;
    }
}