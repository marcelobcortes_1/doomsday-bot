<?php
use Phpfastcache\Config\ConfigurationOption;
use \DrewM\MailChimp\MailChimp;
use Longman\TelegramBot\Request;

class Notification {

    private $asteroid = NULL;
    private $MailChimp = NULL;
    private $token = NULL;
    private $username = NULL;

    public function __construct($a)
    {
        global $mailchimp_token;
        global $token;
        global $username;

        $this->token = $token;
        $this->username = $username;
        $this->asteroid = $a;
        $this->MailChimp = new MailChimp($mailchimp_token);
        $this->Subscription = new Subscription();
    }

    public function send()
    {
        $subscriptors = $this->Subscription->getSubscriptors();
        if (count($subscriptors) > 0){
            if ($this->Subscription->hasEmailSubscriptors()){
                $this->sendEmail();
            }
            $this->sendTelegram($subscriptors);
        }
    }

    private function sendTelegram($subscriptors)
    {
        $msg = $this->asteroid->getAsteroidsMessage();
        try {
            $telegram = new Longman\TelegramBot\Telegram($this->token, $this->username);
            foreach($subscriptors as $chat_id){
                Request::sendMessage([
                    'chat_id' => $chat_id,
                    'text'    => $msg,
                ]);
            }
        } catch (Longman\TelegramBot\Exception\TelegramException $e) {
            Longman\TelegramBot\TelegramLog::error($e);
        } catch (Longman\TelegramBot\Exception\TelegramLogException $e) {
            Longman\TelegramBot\TelegramLog::error($e);
        }
    }
    
    private function sendEmail()
    {
        $emailList = $this->Subscription->getEmailList();
        $campaign_id = $this->MailChimp->post("/campaigns", [
            'type' => 'regular',
            'recipients' => [
                'list_id' => $emailList
            ],
            'settings' => [
                'subject_line' => 'Beware! Asteroids are coming',
                'from_name' => 'Fake Company LTDA',
                'reply_to' => 'noreply@mail.com'
            ]
        ]);

        if ($this->MailChimp->success()){
            $this->updateCampaignContent($campaign_id['id']);
        } else {
            error_log(json_encode($this->MailChimp->getLastError()));
        }
    }

    private function updateCampaignContent($campaign_id)
    {
        $html = $this->asteroid->getAsteroidsMessageHTML();

        $this->MailChimp->put("/campaigns/$campaign_id/content", [
            'html' => $html
        ]);
        $this->sendCampaign($campaign_id);
    }

    private function sendCampaign($campaign_id)
    {
        $this->MailChimp->post("/campaigns/$campaign_id/actions/send", []);
    }

}

?>